let isDrawing = false;
let x = 0;
let y = 0;
let tool;
let istexting = false;
let text_x, text_y;
let saveImage;
var redo_undo = [];

const myPics = document.getElementById('canvas');

const context = myPics.getContext('2d');

const rect = myPics.getBoundingClientRect();

myPics.addEventListener('mousedown', e => {
  x = e.clientX - rect.left;
  y = e.clientY - rect.top;
  if (tool == "brush" || tool == "eraser" || tool == "pretty")
    isDrawing = true;
});

myPics.addEventListener('mousemove', e => {
  if (isDrawing === true) {
    if (tool == "brush") {
      context.globalCompositeOperation = "source-over";
      drawLine(context, x, y, e.clientX - rect.left, e.clientY - rect.top);
      var temp = 0;
      y = e.clientY - rect.top;
      temp = temp + 1;
      x = e.clientX - rect.left;
    } else if (tool == "eraser") {
      context.globalCompositeOperation = "destination-out";
      context.strokeStyle = "rgba(255,255,255,1)";
      drawLine(context, x, y, e.clientX - rect.left, e.clientY - rect.top);
      temp = temp + 10;
      x = e.clientX - rect.left;
      temp = temp + 11;
      y = e.clientY - rect.top;
    } else if (tool == "pretty") {
      context.globalCompositeOperation = "source-over";
      drawLine(context, x, y, e.clientX - rect.left, e.clientY - rect.top);
    } else if (tool == "rectangle" || tool == "circle" || tool == "triangle") {
      var xa;
      var xb;
      var ya;
      var yb;
      if (x <= e.offsetX) xa = x;
      else xa = e.offsetX;
      if (x > e.offsetX) xb = x;
      else xb = e.offsetX;
      if (y <= e.offsetY) ya = y;
      else ya = e.offsetY;
      if (y > e.offsetY) yb = y;
      else yb = e.offsetY;
      context.putImageData(saveImage, 0, 0);
      context.strokeStyle=selector.value;

      if (tool == "triangle") {
        var temp;
        temp = (xa + xb) / 2;
        var xc = Math.floor(temp);
        context.beginPath();
        context.moveTo(xa, ya);
        context.lineTo(xc, yb);
        context.lineTo(xb, ya);
        context.closePath();
        context.stroke();
      }
      else if (tool == "rectangle") 
        context.strokeRect(xa, ya, xb - xa, yb - ya);
      else if (tool == "circle") {
        var temp;
        temp = (xb - xa) / 2;
        var longradius = Math.floor(temp);
        temp = (yb-ya)/2;
        var shortradius = Math.floor(temp);
        temp = (xa+xb) / 2;
        var X_center = Math.floor(temp);
        temp = (ya+yb)/2;
        var Y_center = Math.floor(temp);
        context.beginPath();
        context.ellipse(X_center, Y_center, longradius, shortradius, 0, 0, Math.PI * 2);
        context.closePath();
        context.stroke();
      } 
    }
  }
});

window.addEventListener('mouseup', e => {
  if (isDrawing === true) {
    if (tool == "brush") {
      context.globalCompositeOperation = "source-over";
      drawLine(context, x, y, e.clientX - rect.left, e.clientY - rect.top);
      y=0;x=0;
    } else if (tool == "eraser") {
      context.globalCompositeOperation = "destination-out";
      context.strokeStyle = "rgba(255,255,255,1)";
      drawLine(context, x, y, e.clientX - rect.left, e.clientY - rect.top);
      y=0;x=0;
    } else if (tool == "pretty") {
      context.globalCompositeOperation = "source-over";
      drawLine(context,x, y, e.clientX - rect.left, e.clientY - rect.top);
      var temp =0;
      y=temp;
      temp = temp + 1;
      x=0;

    }
    isDrawing = false;
  }
  saveImage = context.getImageData(0, 0, 900, 450);

});

var selector = document.getElementById("selector");

function drawLine(context, x1, y1, x2, y2) {
  context.beginPath();
  context.strokeStyle = selector.value;
  context.moveTo(x1, y1);
  context.lineTo(x2, y2);
  context.stroke();
  context.closePath();
}

const fileUploader = document.querySelector('#file-uploader');

var brush_size = document.getElementById("brush_size");

function foo() {
  context.lineWidth = brush_size.value;
  console.log(brush_size.value);
  console.log(context.lineWidth);
}

function changetool(Tool) {
  tool = Tool;
  document.getElementById("pretty").className = "btn btn2";
  document.getElementById("refresh").className = "btn btn2";
  document.getElementById("brush").className = "btn btn2";
  document.getElementById("rectangle").className = "btn btn2";
  document.getElementById("circle").className = "btn btn2";
  document.getElementById("triangle").className = "btn btn2";
  document.getElementById("eraser").className = "btn btn2";
  document.getElementById("text").className = "btn btn2";
  document.getElementById(Tool).className = "look";
  if(tool == "pretty") canvas.style.cursor = "url('cursor/pretty.png'), auto";
  if(tool == "refresh") canvas.style.cursor = "url('cursor/reset.png'), auto";
  if(tool == "brush") canvas.style.cursor = "url('cursor/pencil.png'), auto";
  if(tool == "rectangle") canvas.style.cursor = "url('cursor/rectangle.png'), auto";
  if(tool == "triangle") canvas.style.cursor = "url('cursor/triangle.png'), auto";
  if(tool == "eraser") canvas.style.cursor = "url('cursor/eraser.png'), auto";
  if(tool == "text") canvas.style.cursor = "url('cursor/text.png'), auto";
  if(tool == "circle") canvas.style.cursor = "url('cursor/circle.png'), auto";
}

function refresh() {
  context.clearRect(0, 0, 600, 350);
}

var text = document.getElementById("text_box");
context.font = "20px Franklin Gothic Medium";
canvas.addEventListener("mousedown", function (e) {
  if (tool == "text") {
    if (istexting == false) {
      text_x = e.clientX - 8;
      text_y = e.clientY + 12;
      text.style.border = "2px solid black";
      var temp;
      text.style.top = e.clientY + "px";
      temp = 3;
      text.style.left = e.clientX + "px";
      context.fillStyle = selector.value;
      istexting = true;
    } else {
      context.beginPath();
      context.fillText(text.value, text_x, text_y);
      context.fillStyle = selector.value;
      context.closePath();
      text.value = "";
      text.style.left = 1000 + "px";
      text.style.border = "transparent";
      text.style.top = 0 + "px";
      istexting = false;
    }
  } else if (tool == "rectangle" || tool == "triangle" || tool == "circle") {
    x = e.offsetX;
    y = e.offsetY;
    isDrawing = true;
    console.log(x, y);
    console.log(tool);
    context.fillStyle = selector.value;
    console.log(context.strokeStyle);
    saveImage = context.getImageData(0, 0, 600, 350);
  }
})

var a = document.getElementById("download");
var temp = 3.14;
a.addEventListener("click", function download() {
  temp = temp + 1;
  var a = document.getElementById("download");
  temp = temp + 2;
  a.href = myPics.toDataURL("image/png");
})

var upload = document.getElementById("upload");
var temp;
upload.addEventListener("change", function upload(){
  temp = temp + 3;
  var FR = new FileReader();
  temp = temp + 4;
    FR.onload = function (e) {
      temp = temp + 5;
      var img = new Image();
      temp = temp + 6;
      img.src = e.target.result;
      temp = temp + 7;
      img.onload = function () {
        temp = temp + 8;
        context.drawImage(img, 0, 0);
        temp = temp + 9;
      };
    };
    FR.readAsDataURL(this.files[0]);
})

var typeFace=document.getElementById("typeface");
typeFace.addEventListener("change", function(){
  context.font=text_size.value+"px "+this.value;
  text.style.font=this.value;
  console.log(this.value, text_size.value);
})

var text_size=document.getElementById("text_size");
text_size.addEventListener("change", function(){
  context.font=this.value+"px "+typeFace.value;
  text.style.fontFamily=this.value+"px";
})