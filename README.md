# Software Studio 2021 Spring
## Assignment 01 Web Canvas


### Scoring

| **Basic components**                             | **Score** | **Check** |
| :----------------------------------------------- | :-------: | :-------: |
| Basic control tools                              | 30%       | Y         |
| Text input                                       | 10%       | Y         |
| Cursor icon                                      | 10%       | Y         |
| Refresh button                                   | 10%       | Y         |

| **Advanced tools**                               | **Score** | **Check** |
| :----------------------------------------------- | :-------: | :-------: |
| Different brush shapes                           | 15%       | Y         |
| Un/Re-do button                                  | 10%       | Y         |
| Image tool                                       | 5%        | Y         |
| Download                                         | 5%        | Y         |

| **Other useful widgets**                         | **Score** | **Check** |
| :----------------------------------------------- | :-------: | :-------: |
| Name of widgets                                  | 1~5%     | N         |


---

### How to use 

![](https://i.imgur.com/2WhH5rM.png)
這是剛進入小畫家介面。畫布在上方，按鈕、各個功能在下方。

![](https://i.imgur.com/ky9PlVy.png)
第一個功能是brush，按下brush時，brush按鈕會變比較大，且按鈕中字體反白，讓使用者明顯知道現在的狀態是brush。
此功能是模擬鉛筆或筆刷，用單色畫畫。
下方有選項可以選擇brush的顏色及粗細，會在下方介紹。

![](https://i.imgur.com/5nD3KdT.png)
第二個功能是eraser，按下按鈕時按鈕一樣會有反應，下方一樣有橡皮擦的粗細可以選擇。
此功能是模擬橡皮擦，可擦去不要的圖畫。

![](https://i.imgur.com/7dOGQTR.png)
第三個功能是text，按下按鈕時按鈕一樣會有反應，下方有字型、字體大小及字體顏色可供選擇。下方會介紹。
此功能是可以讓使用者打想要的字到畫布上。

![](https://i.imgur.com/hYIMtae.png)
第四個功能是refresh，按下按鈕時按鈕只會有框框的反應，不會變大，變大的按鈕仍然停留在上格按鈕，原因是使用者只要按一下refresh就能完全清空，按完便馬上要回去用brush或其他功能畫畫，如此設計可讓使用者不用按完refresh又須再按一次brush或其他功能才能繼續畫畫。
此功能可讓畫布完全清空。

第五個功能是pretty是bonus，會在function description介紹。

![](https://i.imgur.com/Vvm6HSe.png)
第六個功能是circle，按下按鈕時按鈕一樣會有反應，下方有顏色及線條粗細可供選擇。
此功能可畫出任意大小的圓形或橢圓形。

![](https://i.imgur.com/xxYx3r7.png)
第七個功能是rectangle，按下按鈕時按鈕一樣會有反應，下方有顏色及線條粗細可供選擇。
此功能可畫出任意大小的正方形或長方形。

![](https://i.imgur.com/YGW9izQ.png)
第八個功能triangle，按下按鈕時按紐一樣會有反應，下方有顏色及線條粗細可供選擇。
此功能可畫出任意大小的三角形。

第九及第十個功能，我沒有做好，所以無法使用，不好意思。

接著要介紹下方輔助的功能。

![](https://i.imgur.com/w3c9Z4L.png)
第一個是color_selector，按下方形時，會出現顏色選擇器，是漸層的，而且可選擇任何顏色，或輸入RGB指定顏色。
此功能可以配合brush、text、pretty、circle、rectangle、triangle改變顏色。
在使用下方輔助功能時，上方按鈕仍保持使用狀態，如triangle按鈕仍明顯較大，表示選完顏色後能立即至畫布上作畫，不須再次按triangle才能作畫。

![](https://i.imgur.com/wrhVC0g.png)
第二個功能是typeface，提供三種字體給使用者作選擇，此功能可搭配text做使用。

![](https://i.imgur.com/uZKUvla.png)
第三及第四個功能是brush_size及text_size，可調整線條的粗細或字體的大小，brush_size的range為1至25，預設為3；text_size的range為10至50，預設為12。
brush_size搭配brush、eraser、pretty、circle、rectangle、triangle使用；text_size搭配text使用。

![](https://i.imgur.com/3Y6bkuU.png)
第五個功能是upload，按下選擇檔案時可以選擇電腦中的任意檔案並上傳到畫布上，且選擇檔案右邊會顯示當前上傳的檔名。
此功能可讓使用者上傳喜歡的圖片當背景，且仍然可以在圖片上作畫或打字。

![](https://i.imgur.com/8yy6uvA.png)
第六個功能是download，按下download可以下載當時的畫布到電腦中。
此功能可讓使用者儲存畫好的畫至電腦上。

使用上方各種功能時，游標圖形會隨著功能不同而有不同的圖形。

### Function description

![](https://i.imgur.com/FBgN7AR.png)
此功能為上方第五個功能，使用者按下滑鼠左鍵後，將會設下固定的一點，接著持續按著左鍵便能圍繞固定那點畫出無數的直線線條，直到放開左鍵。此功能搭配下方color_selector及brush_size做使用。
作法:mousedown時，記下x,y座標，進入mousemove後，持續不斷畫出最短路徑(也就是直線)，直到mouseup。

### Gitlab page link

https://108062303.gitlab.io/AS_01_WebCanvas

### Others (Optional)
    
    Anythinh you want to say to TAs.
    
    Nothing

<style>
table th{
    width: 100%;
}
</style>

